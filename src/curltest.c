#include <stdio.h>
#include <curl/curl.h>
#include <unistd.h>
#include <string.h>

CURLM *multi;


static size_t wr (char *d, size_t n, size_t nemb, void *p)
{
    char buf[1024];
    printf(" ** WRITE cb called n=%ld nemb=%ld\n", n, nemb);
    memcpy(buf, d, n*nemb);
    buf[n*nemb] = 0;
    printf("WR data is: %s\n", buf);
    return 1;
}

static size_t rd (char *d, size_t n, size_t nemb, void *p)
{
    printf(" ** READ cb called n=%ld cnt=%ld\n", n, nemb);
    *d = 'a';
    return 1;
}

static void init (char *data)
{
    CURL *eh;
    struct curl_slist *headers = NULL;
    printf("Preparint to send %s\n", data);
    headers = curl_slist_append(headers, "Connection: keep-alive");
    eh = curl_easy_init();
    curl_easy_setopt(eh, CURLOPT_WRITEFUNCTION, wr);
    //curl_easy_setopt(eh, CURLOPT_READFUNCTION, rd);
    curl_easy_setopt(eh, CURLOPT_HEADER, 0L);
    curl_easy_setopt(eh, CURLOPT_URL, "http://127.0.0.1:5000/curl");
    //curl_easy_setopt(eh, CURLOPT_URL, "http://epfl-lab-mon.cisco.com");
    curl_easy_setopt(eh, CURLOPT_VERBOSE, 0L);
    curl_easy_setopt(eh, CURLOPT_POST, 1L);
    curl_easy_setopt(eh, CURLOPT_POSTFIELDSIZE, strlen(data));
    curl_easy_setopt(eh, CURLOPT_POSTFIELDS, data);
    curl_easy_setopt(eh, CURLOPT_HTTPHEADER, headers);

    curl_multi_add_handle(multi, eh);

}

void do_transaction(void)
{
    fd_set R,W,E;
    long L;
    int Q;
    struct timeval T;
    CURLMsg *msg;
    int cnt;
    int M;
    CURLcode rc;


    printf("Waiting for results!\n");

    cnt = 1;
    while (cnt) {
        rc = curl_multi_perform(multi, &cnt);
        //printf("from curl_multi_perform rc=%d cnt=%d\n", rc, cnt);
        if (cnt) {
            FD_ZERO(&R);
            FD_ZERO(&W);
            FD_ZERO(&E);
            //puts("Calling fdset");
            curl_multi_fdset(multi, &R, &W, &E, &M);
            //printf("M es %d\n", M);
            if (M==-1) {
                sleep(1);
            }
            //puts("Calling curl_multi_timeout");
            if (curl_multi_timeout(multi, &L)) {
                printf("TIMEOUT!!!\n");
                return ;
            }
            //printf("L is %ld\n", L);
            T.tv_sec = L/1000;
            T.tv_usec = (L%1000)*1000;
            //puts("Enter select");
            select(M+1, &R, &W, &E, &T);
            //puts("Exit select");
            
            while((msg = curl_multi_info_read(multi, &Q))) {
                if (msg->msg == CURLMSG_DONE) {
                    printf("Done, result=%d - %s", msg->data.result, curl_easy_strerror(msg->data.result));
                    curl_multi_remove_handle(multi, msg->easy_handle);
                    curl_easy_cleanup(msg->easy_handle);
                } else {
                    printf("Otro mensaje!\n");
                }
            }

        }
    }
    printf("CNT es 0, end...\n");
    while((msg = curl_multi_info_read(multi, &Q))) {
        if (msg->msg == CURLMSG_DONE) {
            long code;
            //printf("Done, result=%d - %s\n", msg->data.result, curl_easy_strerror(msg->data.result));
            curl_easy_getinfo(msg->easy_handle, CURLINFO_RESPONSE_CODE, &code);
            printf("HTTP code=%ld\n", code);
            curl_multi_remove_handle(multi, msg->easy_handle);
            curl_easy_cleanup(msg->easy_handle);
        } else {
            printf("Otro mensaje!\n");
        }
    }


}
int main(void)
{
    printf("** EXEC **\n");

    CURLcode rc;
    rc = curl_global_init(CURL_GLOBAL_ALL);
    multi = curl_multi_init();
    printf("Rc=%d\n", rc);
    curl_multi_setopt(multi, CURLMOPT_MAX_TOTAL_CONNECTIONS, 1L);

    init("Hola 1");
    init("Hola 2");

    do_transaction();

    curl_multi_cleanup(multi);
    return 0;
}